package diddi.util.arg;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class IntArgument extends Argument<Integer> {
    private final Pattern pattern;

    public IntArgument(String longName, String shortName) {
        super(longName, shortName);

        if (this.shortName != null)
            pattern = Pattern.compile(String.format("(%s|%s)=(\\d*)", this.shortName, this.longName));
        else
            pattern = Pattern.compile(String.format("(%s)=(\\d*)", this.longName));
    }

    @Override
    public Set<Integer> Fetch(String argString) {
        var m = pattern.matcher(argString);

        return m.results()
                .map(r -> r.group(2))
                .map(Integer::valueOf)
                .collect(Collectors.toSet());
    }
}
