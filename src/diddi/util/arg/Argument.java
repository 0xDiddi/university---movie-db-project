package diddi.util.arg;

import java.util.Set;

abstract class Argument<T> {
    final String longName;
    final String shortName;

    Argument(String longName, String shortName) {
        this.longName = "--" +  longName.replace(" ", "-");
        if (shortName != null) this.shortName = "-" + shortName.replace(" ", "-");
        else this.shortName = null;
    }

    // only the implementation is used. I'm not specifically invoking this general method
    @SuppressWarnings("unused")
    public abstract Set<T> Fetch(String argString);
}
