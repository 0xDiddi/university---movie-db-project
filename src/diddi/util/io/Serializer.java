package diddi.util.io;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Serializer {

    /**
     * Serializes an object(*) into a String that can be saved to a file.
     *
     * (*) Valid objects are:
     *    - String
     *    - native types (numbers + boolean)
     *    - Maps
     *    - Sets
     *    - Objects which have their fields annotated with @Serialize
     * @param object The object to serialize
     * @return The string to save to disk
     */
    public static String serialize(Object object) {
        var sb = new StringBuilder();
        serializeInternal("root", sb, object);
        return sb.toString();
    }

    private static void serializeInternal(String prefix, StringBuilder sb, Object object) {
        if (object instanceof String || object instanceof Number || object instanceof Boolean) {
            sb.append(String.format("%s.@type=%s\n", prefix, object.getClass().getName()));
            sb.append(String.format("%s.value=%s\n", prefix, object));
        } else if (object instanceof Set) {
            sb.append(String.format("%s.@type=set\n", prefix));
            var map = (Set) object;
            for (var o : map) serializeInternal(prefix + "." + o.hashCode(), sb, o);
        } else if (object instanceof Map) {
            sb.append(String.format("%s.@type=map\n", prefix));
            // for some reason, when we just cast to Map (without any type parameters), o will be an object instead of an entry
            //noinspection unchecked
            var map = (Map<Object, Object>) object;
            for (var o : map.entrySet()) {
                var k = o.getKey().hashCode();
                serializeInternal(prefix + "." + k + ".key", sb, o.getKey());
                serializeInternal(prefix + "." + k + ".value", sb, o.getValue());
            }
        } else {
            var clazz = object.getClass();
            sb.append(String.format("%s.@type=%s\n", prefix, object.getClass().getName()));

            for (var field : clazz.getDeclaredFields()) {
                if (!field.isAnnotationPresent(Serialize.class)) continue;
                try {
                    field.setAccessible(true);
                    var key = field.getAnnotation(Serialize.class).value();
                    var val = field.get(object);
                    serializeInternal(String.format("%s.%s", prefix, key), sb, val);
                } catch (IllegalAccessException ignored) {
                    // as we always set the accessibility to true, this _should_ never happen
                }
            }
        }
    }

    /**
     * Tries to deserialize an object from a reader.
     * @param reader The reader to read the string from
     * @param clazz The class of the resulting object
     * @param <T> The type of the resulting object
     * @return The deserialized object
     * @throws SerializationException If the input string is invalid, or the deserialized object is not of type T
     */
    public static <T> T deserialize(BufferedReader reader, Class<T> clazz) throws SerializationException {
        var kv = reader.lines()
                .map(l -> {
                    var s = l.split("=");
                    var key = s[0];
                    var val = s[1];

                    return new Pair<>(key, val);
                }).collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        var o = deserializeInternal(kv);
        try {
            return clazz.cast(o);
        } catch (ClassCastException e) {
            throw new SerializationException("Deserialized content does not conform to target type.");
        }
    }

    private static Map<String, String> eatOnePrefix(Map<String, String> lines) {
        return lines.entrySet().stream()
                .map(l -> {
                    var nk = l.getKey().substring(l.getKey().indexOf(".") + 1);
                    return new Pair<>(nk, l.getValue());
                }).collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    private static Object deserializeInternal(Map<String, String> lines) throws SerializationException {
        var nl = eatOnePrefix(lines);

        var natives = Set.of("java.lang.Byte", "java.lang.Short", "java.lang.Integer", "java.lang.Long",
                "java.lang.Float", "java.lang.Double", "java.lang.Boolean");

        String s = nl.get("@type");
        if (s.equals("java.lang.String")) {
            return nl.get("value");
        } else if (natives.contains(s)) {
            try {
                var clazz = Class.forName(s);
                var m = clazz.getMethod("valueOf", String.class);
                return m.invoke(null, nl.get("value"));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassNotFoundException ignored) {
                // NoSuchMethod: all native type wrappers have the valueOf(String) method.
                // InvocationTarget: the method is static, so no target necessary
                // IllegalAccess: the method is public, there shouldn't be a problem
                // ClassNotFound: we are doing this only for native types. If those aren't there, we might have bigger problems
                return null;
            }
        } else if (s.equals("set")) {
            return nl.entrySet().stream()
                    .filter(kv -> !kv.getKey().equals("@type"))
                    .collect(Collectors.groupingBy(kv -> kv.getKey().substring(0, kv.getKey().indexOf(".")),
                            Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
                    .values().stream()
                    .map(i -> {
                        try {
                            return deserializeInternal(i);
                        } catch (SerializationException e) {
                            // the only reasonable thing to do here is to pass the exception up & out of the lambda.
                            // the only way to do this is to wrap it into a RuntimeException and thereby bypass needing to catch it.
                            // also, if checked exceptions weren't a thing, this could be a nice one-liner.
                            throw new RuntimeException(e);
                        }
                    }).collect(Collectors.toSet());
        } else if (s.equals("map")) {
            return nl.entrySet().stream()
                    .filter(kv -> !kv.getKey().equals("@type"))
                    .collect(Collectors.groupingBy(kv -> kv.getKey().substring(0, kv.getKey().indexOf(".")),
                            Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
                    .values().stream()
                    .map(i -> {
                        try {
                            var sl = eatOnePrefix(i).entrySet().stream()
                                    .collect(Collectors.groupingBy(kv -> kv.getKey().substring(0, kv.getKey().indexOf(".")),
                                            Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
                            return new Pair<>(deserializeInternal(sl.get("key")), deserializeInternal(sl.get("value")));
                        } catch (SerializationException e) {
                            // same as for sets, we need to get out of the lambda
                            throw new RuntimeException(e);
                        }
                    }).collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        } else {
            Class<?> clazz = null;
            Object object = null;
            try {
                clazz = Class.forName(s);
                var ctor = clazz.getDeclaredConstructor();
                ctor.setAccessible(true);
                object = ctor.newInstance();
            } catch (NoSuchMethodException | InstantiationException e) {
                throw new SerializationException("A Serializable class mustn't be abstract and needs to have a default constructor.");
            } catch (IllegalAccessException ignored) {
                // we explicitly allow access via setAccessible(true). this shouldn't happen.
            } catch (InvocationTargetException e) {
                throw new SerializationException("A serializable class mustn't throw in its constructor.");
            } catch (ClassNotFoundException e) {
                throw new SerializationException("Class '" + s + "' not found.");
            }
            var sl = nl.entrySet().stream()
                    .filter(kv -> !kv.getKey().equals("@type"))
                    .collect(Collectors.groupingBy(kv -> kv.getKey().substring(0, kv.getKey().indexOf(".")),
                            Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
            for (var field : clazz.getDeclaredFields()) {
                if (!field.isAnnotationPresent(Serialize.class)) continue;
                try {
                    field.setAccessible(true);
                    var key = field.getAnnotation(Serialize.class).value();
                    var val = deserializeInternal(sl.get(key));

                    field.set(object, val);
                } catch (IllegalAccessException ignored) {
                    // same as for serialize
                    return null;
                }
            }
            return object;
        }
    }

    public static class SerializationException extends Exception {
        SerializationException(String s) {
            super(s);
        }
    }
}
