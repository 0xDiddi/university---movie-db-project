package diddi.interactive_mode;

import diddi.db.DB;
import diddi.db.model.DbActor;
import diddi.db.model.DbDirector;
import diddi.db.model.DbGenre;
import diddi.db.model.DbMovie;
import diddi.util.io.FileException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The interactive mode is designed as a state machine.
 * This is its state transition table.
 * <p>
 * ┌────────┬─────────┬─────────────────────┬───────────────────────┬──────────────────────┬───────────────┬────────────────────┬────────────────────┬─────────┬──────────────────┐
 * │ v SRC  │  DEST > │     MODE_SELECT     │     QUERY_MASTER      │    SEARCH_SELECT     │   SEARCH_*    │    RESULT_LIST     │   MOVIE_SELECTED   │ INVALID │      FINISH      │
 * ├────────┴─────────┼─────────────────────┼───────────────────────┼──────────────────────┼───────────────┼────────────────────┼────────────────────┼─────────┼──────────────────┤
 * │ MODE_SELECT      │                     │ "input=""query"""     │ "input=""search"""   │               │                    │                    │ other   │ "input=""exit""" │
 * │ QUERY_MASTER     │                     │                       │ "input=""add_term""" │               │ "input=""show"""   │                    │ other   │                  │
 * │ SEARCH_SELECT    │                     │                       │                      │ "input=""*""" │                    │                    │ other   │                  │
 * │ SEARCH_*         │                     │ data instanceof query │                      │               │ data=null          │                    │ other   │                  │
 * │ RESULT_LIST      │                     │                       │                      │               │ "input=""paging""" │ "input=""index"""  │ other   │                  │
 * │ MOVIE_SELECTED   │ "input=""restart""" │                       │                      │               │                    │ "input=""rating""" │ other   │ "input=""done""" │
 * │ INVALID          │ always              │                       │                      │               │                    │                    │         │                  │
 * └──────────────────┴─────────────────────┴───────────────────────┴──────────────────────┴───────────────┴────────────────────┴────────────────────┴─────────┴──────────────────┘
 */

public class InteractiveMode {
    /**
     * Starts interactive mode, which allows the user to perform numerous queries or searches
     */
    public static void start(DB db) {
        InteractiveMode.db = db;
        input = new BufferedReader(new InputStreamReader(System.in));

        StateMachineStepData data = new StateMachineStepData();
        data.state = State.MODE_SELECT;

        do stateMachineStep(data); while (data.state != State.FINISH);
    }

    private static class StateMachineStepData {
        State state;
        Object data;
    }

    private static DB db;
    private static BufferedReader input;

    private static void stateMachineStep(StateMachineStepData data) {
        String inputLine;
        switch (data.state) {
            case MODE_SELECT:
                inputLine = queryInput("Make a [query] to get recommendations, perform a simple [search], get [recommendations] or [exit]: ");
                if (inputLine.equalsIgnoreCase("query"))
                    data.state = State.QUERY_MASTER;
                else if (inputLine.equalsIgnoreCase("search"))
                    data.state = State.SEARCH_SELECT;
                else if (inputLine.equalsIgnoreCase("exit"))
                    data.state = State.FINISH;
                else if (inputLine.equals("recommendations")) {
                    data.data = db.query().ratedLike(db.getRatedMovies()).showRecommendations();
                    data.state = State.MOVIE_RESULT_LIST;
                }
                else System.out.println("Please select either 'query', 'search', or 'exit'");
                break;
            case QUERY_MASTER:
                if (!(data.data instanceof DB.Query)) data.data = db.query();
                inputLine = queryInput("[add] a term to your query or [show] the results: ");

                if (inputLine.equalsIgnoreCase("add"))
                    data.state = State.SEARCH_SELECT;
                else if (inputLine.equalsIgnoreCase("show")) {
                    data.state = State.MOVIE_RESULT_LIST;
                    data.data = ((DB.Query) data.data).showRecommendations();
                } else System.out.println("Please select either 'add' or 'show'.");
                break;
            case SEARCH_SELECT:
                boolean isInQuery = data.data instanceof DB.Query;
                var q = isInQuery ? "Add a term to the query: [movie|actor|director|genre|limit]: " :
                        "Select whether to search for [movie]-title, [actor], [director] or [genre]: ";

                inputLine = queryInput(q);

                if (inputLine.equalsIgnoreCase("movie"))
                    data.state = State.SEARCH_MOVIE;
                else if (inputLine.equalsIgnoreCase("actor"))
                    data.state = State.SEARCH_ACTOR;
                else if (inputLine.equalsIgnoreCase("director"))
                    data.state = State.SEARCH_DIRECTOR;
                else if (inputLine.equalsIgnoreCase("genre"))
                    data.state = State.SEARCH_GENRE;
                else if (inputLine.equalsIgnoreCase("limit") && isInQuery) {
                    inputLine = queryInput("Maximum number of recommendations: ");
                    try {
                        ((DB.Query) data.data).limit(Integer.parseInt(inputLine));
                    } catch (NumberFormatException ignored) {
                        System.out.println("Input was not a number.");
                    }
                    data.state = State.QUERY_MASTER;
                } else {
                    var e = isInQuery ? "Please enter either 'movie', 'actor', 'director' or 'limit'." :
                            "Please enter either 'movie', 'actor' or 'director'.";
                    System.out.println(e);
                }
                break;
            case SEARCH_MOVIE:
                inputLine = queryInput("Enter movie name: ");
                var movies = db.findMovies(Collections.singleton(inputLine));

                if (data.data instanceof DB.Query) {
                    System.out.println("Found the following movies: " + Arrays.toString(movies.toArray()));
                    data.state = State.QUERY_MASTER;
                    ((DB.Query) data.data).ratedLike(movies);
                } else {
                    data.state = State.MOVIE_RESULT_LIST;
                    data.data = movies;
                }
                break;
            case SEARCH_ACTOR:
                inputLine = queryInput("Enter actor name: ");
                var actors = db.findActors(Collections.singleton(inputLine));

                if (data.data instanceof DB.Query) {
                    System.out.println("Found the following actors: " + Arrays.toString(actors.toArray()));
                    data.state = State.QUERY_MASTER;
                    ((DB.Query) data.data).preferActor(actors);
                } else {
                    data.state = State.MOVIE_RESULT_LIST;
                    data.data = actors.stream()
                            .map(DbActor::getRelatedMovies)
                            .flatMap(Set::stream)
                            .collect(Collectors.toSet());
                }
                break;
            case SEARCH_DIRECTOR:
                inputLine = queryInput("Enter director name: ");
                var directors = db.findDirectors(Collections.singleton(inputLine));

                if (data.data instanceof DB.Query) {
                    System.out.println("Found the following directors: " + Arrays.toString(directors.toArray()));
                    data.state = State.QUERY_MASTER;
                    ((DB.Query) data.data).preferDirector(directors);
                } else {
                    data.state = State.MOVIE_RESULT_LIST;
                    data.data = directors.stream()
                            .map(DbDirector::getRelatedMovies)
                            .flatMap(Set::stream)
                            .collect(Collectors.toSet());
                }
                break;
            case SEARCH_GENRE:
                inputLine = queryInput("Enter genre name: ");
                var s_genres = db.findGenres(Collections.singleton(inputLine));

                if (data.data instanceof DB.Query) {
                    System.out.println("Found the following genres: " + Arrays.toString(s_genres.toArray()));
                    data.state = State.QUERY_MASTER;
                    ((DB.Query) data.data).preferGenre(s_genres);
                } else {
                    data.state = State.MOVIE_RESULT_LIST;
                    data.data = s_genres.stream()
                            .map(DbGenre::getRelatedMovies)
                            .flatMap(Set::stream)
                            .collect(Collectors.toSet());
                }
                break;
            case MOVIE_RESULT_LIST:
                MovieListPagingData pageData;
                if (data.data instanceof MovieListPagingData)
                    pageData = (MovieListPagingData) data.data;
                else {
                    pageData = new MovieListPagingData();
                    pageData.itemsPerPage = 10;
                    pageData.offset = 0;

                    // these casts can't be checked accurately thanks to the wonder which is type erasure
                    if (data.data instanceof LinkedHashMap)
                        //noinspection unchecked
                        pageData.movies = (LinkedHashMap<DbMovie, Float>) data.data;
                    else if (data.data instanceof Set)
                        //noinspection unchecked
                        pageData.movies = ((Set<DbMovie>) data.data).stream().collect(Collectors.toMap(k -> k, v -> Float.NaN, (a, b) -> a, LinkedHashMap::new));
                }

                if (pageData.movies.size() == 0) {
                    data.state = State.MODE_SELECT;
                    System.out.println("Search gave no results.");
                    return;
                }

                System.out.println(String.format("Showing items %d to %d (Page %d of %d)\n",
                        pageData.offset + 1,
                        Math.min(pageData.offset + pageData.itemsPerPage, pageData.movies.size()),
                        (pageData.offset / pageData.itemsPerPage) + 1,
                        (int) Math.ceil((pageData.movies.size() - 1) / pageData.itemsPerPage) + 1));

                var itemsOnPage = pageData.movies.entrySet().stream()
                        .skip(pageData.offset).limit(pageData.itemsPerPage)
                        .collect(Collectors.toCollection(LinkedHashSet::new));

                var index = 1;
                for (var m : itemsOnPage) {
                    if (Float.isNaN(m.getValue()))
                        System.out.println(String.format(" (%d) %s", index, m.getKey().getTitle()));
                    else
                        System.out.println(String.format(" (%d) [%3.2f%%] %s", index, m.getValue() * 100f, m.getKey().getTitle()));
                    index++;
                }

                System.out.println();

                do {
                    inputLine = queryInput("Page [+|-], or select a movie by index: ");

                    int i;
                    try {
                        i = Integer.parseInt(inputLine);
                    } catch (NumberFormatException ignored) {
                        // non-numeric input is still valid
                        i = -1;
                    }

                    if (inputLine.equals("+") && (pageData.offset + pageData.itemsPerPage) < pageData.movies.size()) {
                        pageData.offset += pageData.itemsPerPage;
                        data.data = pageData;
                    } else if (inputLine.equals("-") && pageData.offset > 0) {
                        pageData.offset -= pageData.itemsPerPage;
                        if (pageData.offset < 0) pageData.offset = 0;
                        data.data = pageData;
                    } else if (i > 0 && i <= pageData.itemsPerPage) {
                        data.data = itemsOnPage.toArray()[i - 1];
                        data.state = State.MOVIE_SELECTED;
                    } else {
                        System.out.println("Invalid input, please try again.");
                        continue;
                    }
                    break;
                } while (true);
                break;
            case MOVIE_SELECTED:
                // unless the state machine is tampered with, it should only ever get here with the right types.
                //noinspection unchecked
                var movie = ((Map.Entry<DbMovie, Float>) data.data).getKey();

                System.out.println(String.format("%s (%s)\n %s", movie.getTitle(), movie.getRelease(), movie.getPlot()));
                if (!Float.isNaN(movie.getUserRating()))
                    System.out.println(String.format("Your rating: %3.2f%%", movie.getUserRating() * 100f));

                var genres = movie.getGenres().stream().map(DbGenre::getName).collect(Collectors.toSet());
                System.out.println("Genres: " + String.join(", ", genres));

                System.out.println("Directors: ");
                movie.getDirectors()
                        .forEach(d -> System.out.println("\t- " + d.getName()));

                System.out.println("Actors: ");
                movie.getActors()
                        .forEach(a -> System.out.println("\t- " + a.getName()));

                inputLine = queryInput("Start a [new] query, [rate] this movie, or [exit] the program: ");
                if (inputLine.equalsIgnoreCase("exit"))
                    data.state = State.FINISH;
                else if (inputLine.equalsIgnoreCase("rate")) {
                    inputLine = queryInput("Enter a score between 0 and 1 (inclusive): ");
                    try {
                        float num = Float.parseFloat(inputLine);
                        // by throwing an exception here, we can also use the catch statement.
                        if (num < 0 || num > 1) throw new IllegalArgumentException("rating must be between 0 & 1");

                        movie.setUserRating(num);
                        db.trySaveRatings();
                    } catch (NumberFormatException ignored) {
                        System.out.println("You entered an invalid number.");
                    } catch (FileException e) {
                        System.out.println(e.getMessage());
                    }
                } else
                    // we actually don't need to check for 'new' input, as starting again is default behaviour
                    data.state = State.MODE_SELECT;
                break;
            default:
                // this should never happen, as the only state without a case is finish, which should exit the program
                throw new RuntimeException("State not handled");
        }
    }

    private static String queryInput(String query) {
        try {
            System.out.print(query);
            return input.readLine();
        } catch (IOException e) {
            // todo: maybe do something here to let the user know?
            return "";
        }
    }

    private enum State {
        MODE_SELECT, QUERY_MASTER,
        SEARCH_SELECT,
        SEARCH_MOVIE, SEARCH_ACTOR, SEARCH_DIRECTOR, SEARCH_GENRE,
        MOVIE_RESULT_LIST, MOVIE_SELECTED,
        FINISH,
    }

    private static class MovieListPagingData {
        LinkedHashMap<DbMovie, Float> movies;
        int itemsPerPage;
        int offset;
    }
}
