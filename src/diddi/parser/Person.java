package diddi.parser;

public abstract class Person {
    public final int id;
    public final String name;

    Person(int id, String name) {
        this.id = id;
        this.name = name.trim();
    }
}
