package diddi.parser;

import diddi.util.io.FileException;

import java.io.*;
import java.util.*;

// If I were to write this again now, I would probably use the OO model from the get-go.
// But because the parser existed before the new OO model, it seemed easier to continue
// rolling with two models and the transformer instead of rewriting the entire parser.

/**
 * Facilitates parsing of the database file
 */
public class Parser {
    private enum State {
        INVALID, ACTOR, MOVIE, DIRECTOR, A_M_REL, D_M_REL, RATING
    }

    /**
     * Options for the parser
     */
    public static class ParserOpts {
        /**
         * Whether or not to merge the list of genres for movie entries with the same ID
         */
        public boolean mergeGenresOnDuplicateMovieId = false;
    }

    public static class ParserOut {
        public final Map<Integer, Actor> actors = new HashMap<>();
        public final Map<Integer, Director> directors = new HashMap<>();
        public final Map<Integer, Movie> movies = new HashMap<>();

        public final Map<Integer, Set<Integer>> actor_movie_relations = new HashMap<>();
        public final Map<Integer, Set<Integer>> director_movie_relations = new HashMap<>();

        public final Map<Integer, Map<String, Float>> movie_rating_relation = new HashMap<>();
    }

    /**
     * Opens the specified file and tries to parse the database
     * @param dbFile The file to read
     * @param opts The options for parsing
     * @return The parsed database
     * @throws ParserException If the file doesn't contain a database or it is of the wrong format
     * @throws FileException If there is an error while reading the file
     */
    public static ParserOut parse(File dbFile, ParserOpts opts) throws ParserException, FileException {
        // this try is just to handle the reader, exceptions are passed on.
        try (var reader = new BufferedReader(new FileReader(dbFile))) {
            return parse(reader, opts);
        } catch (IOException e) {
            throw FileException.errorOnRead(dbFile);
        }
    }

    public static class ParserException extends Exception {
        ParserException(String s) {
            super("Error while parsing database: " + s);
        }
    }

    /**
     * Tries to parse a database from a BuferedReader
     * @param reader The reader to read the database from
     * @param opts The options for parsing
     * @return The parsed database
     * @throws IOException If there is an error with the reader
     * @throws ParserException If the database is of invalid format
     */
    public static ParserOut parse(BufferedReader reader, ParserOpts opts) throws IOException, ParserException {
        var out = new ParserOut();
        String line;
        var state = State.INVALID;
        while ((line = reader.readLine()) != null) {
            if (line.startsWith("New_Entity: ")) {
                var columns = line.substring(line.indexOf(" ") + 1).split(",");
                if (columns.length < 2) throw new ParserException("Entity descriptor has not enough columns.");
                switch (columns[0]) {
                    case "\"actor_id\"":
                        state = columns[1].equals("\"actor_name\"") ? State.ACTOR : State.A_M_REL;
                        break;
                    case "\"director_id\"":
                        state = columns[1].equals("\"director_name\"") ? State.DIRECTOR : State.D_M_REL;
                        break;
                    case "\"movie_id\"":
                        state = State.MOVIE;
                        break;
                    case "\"user_name\"":
                        state = State.RATING;
                        break;
                    default:
                        state = State.INVALID;
                }
                continue;
            } else if (state == State.INVALID) {
                throw new ParserException("Encountered non entity descriptor line in invalid state.");
            }

            var parts = line.split("\"");
            switch (state) {
                case ACTOR:
                    var a = new Actor(Integer.valueOf(parts[1]), parts[3]);
                    if (out.actors.containsKey(a.id)) throw new ParserException("Duplicate key for actor: " + a.id);
                    out.actors.put(a.id, a);
                    break;
                case DIRECTOR:
                    var d = new Director(Integer.valueOf(parts[1]), parts[3]);
                    if (out.directors.containsKey(d.id))
                        throw new ParserException("Duplicate key for director: " + d.id);
                    out.directors.put(d.id, d);
                    break;
                case MOVIE:
                    var m = new Movie(Integer.valueOf(parts[1]), parts[3], parts[5],
                            Collections.singleton(parts[7]), parts[9]);
                    if (out.movies.containsKey(m.id)) {
                        if (!opts.mergeGenresOnDuplicateMovieId)
                            throw new ParserException("Duplicate key for movie: " + m.id);
                        else {
                            var m_old = out.movies.get(m.id);
                            out.movies.remove(m.id);
                            var genres = new HashSet<String>();
                            genres.addAll(m_old.genre);
                            genres.addAll(m.genre);
                            m = new Movie(m_old.id, m_old.title, m_old.plot, genres, m_old.release);
                        }
                    }
                    out.movies.put(m.id, m);
                    break;
                case A_M_REL:
                    var a_id = Integer.valueOf(parts[1]);
                    var am_id = Integer.valueOf(parts[3]);

                    if (!out.actor_movie_relations.containsKey(a_id))
                        out.actor_movie_relations.put(a_id, new HashSet<>());
                    out.actor_movie_relations.get(a_id).add(am_id);
                    break;
                case D_M_REL:
                    var d_id = Integer.valueOf(parts[1]);
                    var dm_id = Integer.valueOf(parts[3]);

                    if (!out.director_movie_relations.containsKey(d_id))
                        out.director_movie_relations.put(d_id, new HashSet<>());
                    out.director_movie_relations.get(d_id).add(dm_id);
                    break;
                case RATING:
                    var username = parts[1];
                    var rating = Float.valueOf(parts[3]);
                    var r_movie = Integer.valueOf(parts[5]);

                    if (!out.movie_rating_relation.containsKey(r_movie))
                        out.movie_rating_relation.put(r_movie, new HashMap<>());
                    out.movie_rating_relation.get(r_movie).put(username, rating);
                    break;
            }
        }
        return out;
    }
}
