package diddi.static_mode;

import diddi.db.DB;
import diddi.util.arg.IntArgument;
import diddi.util.arg.StringArgument;

public class StaticMode {
    /**
     * Performs a single query against the given database, based on the parameters given argString
     */
    public static void start(String argString, DB db) {
        var argMovie = new StringArgument("film", "f").Fetch(argString);
        var argActor = new StringArgument("actor", "a").Fetch(argString);
        var argDirector = new StringArgument("director", "d").Fetch(argString);
        var argGenre = new StringArgument("genre", "g").Fetch(argString);
        var argLimit = new IntArgument("limit", "l").Fetch(argString);

        var movies = db.findMovies(argMovie);
        var actors = db.findActors(argActor);
        var directors = db.findDirectors(argDirector);
        var genres = db.findGenres(argGenre);

        var q = db.query()
                .ratedLike(movies)
                .preferActor(actors)
                .preferDirector(directors)
                .preferGenre(genres);

        if (argLimit.size() > 0)
            q.limit(argLimit.stream().min(Integer::compareTo).get());

        var recommendations = q.showRecommendations();

        for (var a : recommendations.entrySet()) {
            System.out.println(String.format("%3.2f%%: %s", a.getValue() * 100f, a.getKey().getTitle()));
        }
    }
}
