package diddi.db.filter;

import diddi.db.model.*;
import diddi.util.io.FileException;
import diddi.util.io.Serialize;
import diddi.util.io.Serializer;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Can be used to filter bad entries from the data model
 */
public class ShitInShitOutFilter {
    private Set<DbMovie> movies;
    private Set<DbActor> actors;
    private Set<DbDirector> directors;
    private Set<DbGenre> genres;

    private File blackListFile;

    /**
     * Sets the blacklist file to the given file
     * @throws FileException If an error occurs while writing the default blacklist
     */
    public void setBlacklistFile(String file) throws FileException {
        if (file != null) {
            blackListFile = new File(file);

            // create a default blacklist if a file is specified but doesn't exist already
            if (!blackListFile.exists()) {
                var bl = new BlackList();
                bl.deleteMoviesWithoutElements = true;
                // these match empty strings
                bl.movieNameBlacklist = Set.of("^$");
                bl.moviePlotBlacklist = Set.of("^$");
                // this allows empty string and correct format, but matches everything else
                bl.movieReleaseBlacklist = Set.of("^(?!(^$|(20|19)\\d{2}-\\d{2}-\\d{2})).*$");
                // this requires people to have at least two parts in their name, separated by spaces
                // but allowing quotation, dots and dashes
                bl.actorBlacklist = Set.of("^((?![a-zA-Z.'-]+ [a-zA-Z.'-]+).*)$");
                bl.directorBlacklist = Set.of("^((?![a-zA-Z.'-]+ [a-zA-Z.'-]+).*)$");
                bl.genreBlacklist = Set.of("^$", "(no genres listed)");

                try (var bw = new BufferedWriter(new FileWriter(blackListFile))) {
                    var str = Serializer.serialize(bl);
                    bw.write(str, 0, str.length());
                    bw.flush();
                } catch (IOException e) {
                    throw FileException.errorOnWrite(blackListFile);
                }
            }
        }
    }

    /**
     * Filters the given model by the blacklist given in the previously selected file
     * @param model The model to filter
     * @return The filtered model
     * @throws FileException If an error occurs while reading the blacklist file
     */
    public ModelTransform.OoModel filter(ModelTransform.OoModel model) throws FileException {
        // we need to copy these to make them mutable.
        movies = new HashSet<>(model.movies);
        actors = new HashSet<>(model.actors);
        directors = new HashSet<>(model.directors);
        genres = new HashSet<>(model.genres);

        if (blackListFile != null && blackListFile.exists()) {

            try (var br = new BufferedReader(new FileReader(blackListFile))) {
                var bl = Serializer.deserialize(br, BlackList.class);
                // stage 1 - filter by the blacklist
                //  this should throw out movies with empty title for example.
                stage1(bl);

                // stage 2 - prune references
                //  there will still be references to the just removed elements.
                stage2();

                // stage 3 - cleanup unreferenced
                //  some elements might now be unreferenced, delete those as well.
                stage3(bl);
            } catch (IOException e) {
                throw FileException.errorOnRead(blackListFile);
            } catch (Serializer.SerializationException e) {
                // If the deserializer throws something during normal runtime (as opposed to debugging), the file is bad
                throw FileException.badContent(blackListFile, "Blacklist file contains invalid contents.");
            }
        }

        // stage 4 - do job of IDs for them
        //  there are e.g. multiple different IDs for the same actor, we need to merge those
        stage4();

        return ModelTransform.createModel(movies, actors, directors, genres, model.ratings);
    }

    private void stage1(BlackList bl) {
        movies = movies.stream()
                .filter(m -> bl.movieNameBlacklist.stream().noneMatch(f -> m.getTitle().matches(f)))
                .filter(m -> bl.moviePlotBlacklist.stream().noneMatch(f -> m.getPlot().matches(f)))
                .filter(m -> bl.movieReleaseBlacklist.stream().noneMatch(f -> m.getRelease().matches(f)))
                .collect(Collectors.toSet());

        actors = actors.stream()
                .filter(a -> bl.actorBlacklist.stream().noneMatch(f -> a.getName().matches(f)))
                .collect(Collectors.toSet());

        directors = directors.stream()
                .filter(d -> bl.directorBlacklist.stream().noneMatch(f -> d.getName().matches(f)))
                .collect(Collectors.toSet());

        genres = genres.stream()
                .filter(g -> bl.genreBlacklist.stream().noneMatch(f -> g.getName().matches(f)))
                .collect(Collectors.toSet());
    }

    private void stage2() {
        movies.forEach(m -> {
            m.getActors().retainAll(actors);
            m.getDirectors().retainAll(directors);
            m.getGenres().retainAll(genres);
        });

        actors.forEach(a -> a.getRelatedMovies().retainAll(movies));
        directors.forEach(d -> d.getRelatedMovies().retainAll(movies));
        genres.forEach(g -> g.getRelatedMovies().retainAll(movies));
    }

    private void stage3(BlackList bl) {
        if (bl.deleteMoviesWithoutElements) {
            movies = movies.stream()
                    .filter(m -> m.getActors().size() > 0 && m.getDirectors().size() > 0 && m.getGenres().size() > 0)
                    .collect(Collectors.toSet());
        }

        actors = actors.stream().filter(a -> a.getRelatedMovies().size() > 0).collect(Collectors.toSet());
        directors = directors.stream().filter(d -> d.getRelatedMovies().size() > 0).collect(Collectors.toSet());
        genres = genres.stream().filter(g -> g.getRelatedMovies().size() > 0).collect(Collectors.toSet());
    }

    private <T extends IMovieRelated> Set<T> mergeFunc(Set<T> list, Function<T, String> group, Function<DbMovie, Set<T>> sf) {
        return list.stream()
                .collect(Collectors.groupingBy(group)).values().stream()
                .map(as -> {
                    var a = as.get(0);
                    // if there is only one, there's no need for change
                    if (as.size() == 1) return a;
                    // now merge all relations to the first object
                    for (int i = 1; i < as.size(); i++) a.getRelatedMovies().addAll(as.get(i).getRelatedMovies());
                    // we want to replace all refs to all but the first object with refs to the first object
                    var rm = as.subList(1, as.size());
                    a.getRelatedMovies().forEach(m -> {
                        var lst = sf.apply(m);
                        lst.removeAll(rm);
                        lst.add(a);
                    });
                    return a;
                })
                .collect(Collectors.toSet());
    }

    private void stage4() {
        actors = mergeFunc(actors, DbActor::getName, DbMovie::getActors);
        directors = mergeFunc(directors, DbDirector::getName, DbMovie::getDirectors);
        genres = mergeFunc(genres, DbGenre::getName, DbMovie::getGenres);
    }

    private static class BlackList {
        @Serialize("movie-references")
        boolean deleteMoviesWithoutElements;

        @Serialize("movie-name")
        Set<String> movieNameBlacklist;

        @Serialize("movie-plot")
        Set<String> moviePlotBlacklist;

        @Serialize("movie-release")
        Set<String> movieReleaseBlacklist;

        @Serialize("actors")
        Set<String> actorBlacklist;

        @Serialize("directors")
        Set<String> directorBlacklist;

        @Serialize("genres")
        Set<String> genreBlacklist;
    }
}
