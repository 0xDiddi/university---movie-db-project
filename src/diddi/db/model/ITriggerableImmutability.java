package diddi.db.model;

/**
 * This interface declares a contract that when it's method is called,
 * all collection fields of the class will become immutable for the rest of the lifetime of the object.
 * <p>
 * This is a design choice that trades finality of fields for their immutability.
 *
 * This choice was made because otherwise the filter may need to reconstruct objects multiple times.
 */
public interface ITriggerableImmutability {
    void makeCollectionFieldsImmutable();
}
