package diddi.db.model;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

abstract class DbPerson implements IMovieRelated, ITriggerableImmutability {
    private final int id;
    private final String name;
    private Set<DbMovie> movies;

    DbPerson(int id, String name, Set<DbMovie> movies) {
        this.id = id;
        this.name = name;
        this.movies = movies;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public Set<DbMovie> getRelatedMovies() {
        return movies;
    }

    @Override
    public boolean isRelatedToMovie(DbMovie m) {
        return movies.contains(m);
    }

    @Override
    public void makeCollectionFieldsImmutable() {
        movies = Collections.unmodifiableSet(movies);
    }

    @Override
    public String toString() {
        return "'" + name + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbPerson dbPerson = (DbPerson) o;
        return id == dbPerson.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
