package diddi.db.model;

import java.util.Set;

public class DbActor extends DbPerson {
    public DbActor(int id, String name, Set<DbMovie> movies) {
        super(id, name, movies);
    }
}
