package diddi.db.model;

import java.util.Set;

public class DbDirector extends DbPerson {
    public DbDirector(int id, String name, Set<DbMovie> movies) {
        super(id, name, movies);
    }
}
