package diddi.db.model;

import java.util.Set;

public interface IMovieRelated {
    Set<DbMovie> getRelatedMovies();

    boolean isRelatedToMovie(DbMovie m);
}
