package diddi.db.model;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class DbGenre implements IMovieRelated, ITriggerableImmutability {
    private final String name;
    private Set<DbMovie> movies;

    public DbGenre(String name, Set<DbMovie> movies) {
        this.name = name;
        this.movies = movies;
    }

    public String getName() {
        return name;
    }

    @Override
    public Set<DbMovie> getRelatedMovies() {
        return movies;
    }

    @Override
    public boolean isRelatedToMovie(DbMovie m) {
        return movies.contains(m);
    }

    @Override
    public void makeCollectionFieldsImmutable() {
        movies = Collections.unmodifiableSet(movies);
    }

    @Override
    public String toString() {
        return "'" + name + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbGenre dbGenre = (DbGenre) o;
        return name.equals(dbGenre.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
