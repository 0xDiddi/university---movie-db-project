package diddi.db.model;

import diddi.parser.Parser;
import javafx.util.Pair;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Facilitates the transformation from the data model used by the parser to the oo data model used by the database
 */
public class ModelTransform {
    /**
     * Transforms the given parser model to an oo model
     * @param parsedData The parser model
     * @return The oo model
     */
    public static OoModel transformModel(Parser.ParserOut parsedData) {
        return new OoModel(parsedData);
    }

    /**
     * Creates an oo model from the given list of entries.
     * No further checks are performed.
     * @return The created model
     */
    public static OoModel createModel(Set<DbMovie> movies, Set<DbActor> actors, Set<DbDirector> directors, Set<DbGenre> genres, Set<DbRating> ratings) {
        return new OoModel(
                Collections.unmodifiableSet(movies),
                Collections.unmodifiableSet(actors),
                Collections.unmodifiableSet(directors),
                Collections.unmodifiableSet(genres),
                Collections.unmodifiableSet(ratings)
        );
    }

    /**
     * Holds an object oriented data model
     */
    public static class OoModel {
        public final Set<DbMovie> movies;
        public final Set<DbActor> actors;
        public final Set<DbDirector> directors;
        public final Set<DbGenre> genres;
        public final Set<DbRating> ratings;

        private OoModel(Parser.ParserOut parsedData) {
            var wMovies = parsedData.movies.values().stream()
                    .map(m -> new DbMovie(m.id, m.title, m.plot, m.release))
                    .collect(Collectors.toMap(DbMovie::getId, m -> m));

            this.actors = parsedData.actors.values().stream()
                    .map(a -> {
                        var m = parsedData.actor_movie_relations.get(a.id).stream()
                                .map(wMovies::get)
                                .collect(Collectors.toSet());
                        return new DbActor(a.id, a.name, m);
                    })
                    .collect(Collectors.toUnmodifiableSet());

            this.directors = parsedData.directors.values().stream()
                    .map(d -> {
                        var m = parsedData.director_movie_relations.get(d.id).stream()
                                .map(wMovies::get)
                                .collect(Collectors.toSet());
                        return new DbDirector(d.id, d.name, m);
                    })
                    .collect(Collectors.toUnmodifiableSet());

            // we are going to a map here first as that is roughly O(n) instead of O(n^2) when filtering all movies for each genre
            var wGenre = new HashMap<String, Set<DbMovie>>();
            parsedData.movies.values().forEach(m -> m.genre.forEach(g -> {
                if (!wGenre.containsKey(g)) wGenre.put(g, new HashSet<>());
                wGenre.get(g).add(wMovies.get(m.id));
            }));
            this.genres = wGenre.entrySet().stream()
                    .map(kv -> new DbGenre(kv.getKey(), kv.getValue()))
                    .collect(Collectors.toUnmodifiableSet());

            this.ratings = parsedData.movie_rating_relation.entrySet().stream()
                    .map(kv -> kv.getValue().entrySet().stream()
                            .map(r -> new DbRating(r.getKey(), r.getValue() / 5f, wMovies.get(kv.getKey())))
                            .collect(Collectors.toSet()))
                    .flatMap(Set::stream)
                    .collect(Collectors.toUnmodifiableSet());

            // we are grouping these here, to avoid streaming the entire thing for each movie
            var gGenres = this.genres.stream().flatMap(g -> g.getRelatedMovies().stream().map(m -> new Pair<>(m, g)))
                    .collect(Collectors.groupingBy(Pair::getKey, HashMap::new, Collectors.toSet()));
            var gActors = this.actors.stream().flatMap(g -> g.getRelatedMovies().stream().map(m -> new Pair<>(m, g)))
                    .collect(Collectors.groupingBy(Pair::getKey, HashMap::new, Collectors.toSet()));
            var gDirectors = this.directors.stream().flatMap(g -> g.getRelatedMovies().stream().map(m -> new Pair<>(m, g)))
                    .collect(Collectors.groupingBy(Pair::getKey, HashMap::new, Collectors.toSet()));
            var gRatings = this.ratings.stream().collect(Collectors.groupingBy(DbRating::getMovie, HashMap::new, Collectors.toSet()));

            this.movies = wMovies.values().stream()
                    .peek(m -> {
                        // we still need to stream _a little_ here, to get from the pair we used for grouping to the simple thing
                        Set<DbGenre> genres = gGenres.containsKey(m) ?
                                gGenres.get(m).stream().map(Pair::getValue).collect(Collectors.toSet()) :
                                Collections.emptySet();
                        Set<DbActor> actors = gActors.containsKey(m) ?
                                gActors.get(m).stream().map(Pair::getValue).collect(Collectors.toSet()) :
                                Collections.emptySet();
                        Set<DbDirector> directors = gDirectors.containsKey(m) ?
                                gDirectors.get(m).stream().map(Pair::getValue).collect(Collectors.toSet()) :
                                Collections.emptySet();
                        var ratings = gRatings.getOrDefault(m, Collections.emptySet());

                        m.fillRelations(actors, directors, genres, ratings);
                    })
                    .collect(Collectors.toUnmodifiableSet());
        }

        private OoModel(Set<DbMovie> movies, Set<DbActor> actors, Set<DbDirector> directors, Set<DbGenre> genres, Set<DbRating> ratings) {
            this.movies = movies;
            this.actors = actors;
            this.directors = directors;
            this.genres = genres;
            this.ratings = ratings;
        }
    }
}
