package diddi.db.model;

public class DbRating {
    private final String username;
    private final float rating;
    private final DbMovie movie;

    public DbRating(String username, float rating, DbMovie movie) {
        this.username = username;
        this.rating = rating;
        this.movie = movie;
    }

    public String getUsername() {
        return username;
    }

    public float getRating() {
        return rating;
    }

    public DbMovie getMovie() {
        return movie;
    }
}
