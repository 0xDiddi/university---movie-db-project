package diddi.db;

import diddi.db.model.*;
import diddi.util.io.FileException;
import diddi.util.io.Serializer;
import javafx.util.Pair;

import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The database used for making queries and performing searches
 */
public class DB {
    private final ModelTransform.OoModel model;

    private File ratingFile;

    /**
     * creates a new database with the given data
     *
     * @param model The model to use
     */
    public DB(ModelTransform.OoModel model) {
        this.model = model;

        this.model.movies.forEach(ITriggerableImmutability::makeCollectionFieldsImmutable);
        this.model.actors.forEach(ITriggerableImmutability::makeCollectionFieldsImmutable);
        this.model.directors.forEach(ITriggerableImmutability::makeCollectionFieldsImmutable);
        this.model.genres.forEach(ITriggerableImmutability::makeCollectionFieldsImmutable);
    }

    public void setRatingFile(String path) {
        if (path != null)
            ratingFile = new File(path);
    }

    /**
     * Tries to save the users ratings to a file (if set)
     *
     * @throws FileException If an error occurs while writing the file
     */
    public void trySaveRatings() throws FileException {
        if (ratingFile == null) return;
        var ratings = getRatedMovies().stream()
                .map(m -> new Pair<>(m.getId(), m.getUserRating()))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        var s = Serializer.serialize(ratings);
        try (var bw = new BufferedWriter(new FileWriter(ratingFile))) {
            bw.write(s, 0, s.length());
            bw.flush();
        } catch (IOException e) {
            throw FileException.errorOnWrite(ratingFile);
        }
    }

    /**
     * Tries to read the users ratings from a file (if set)
     *
     * @throws FileException If an error occurs while reading the file
     */
    public void tryReadRatings() throws FileException {
        if (ratingFile == null || !ratingFile.exists()) return;
        try (var br = new BufferedReader(new FileReader(ratingFile))) {
            // thanks to the great thing that is type erasure, there is no way for the deserializer to make sure the type is right
            // at least not the type of type parameters (which is their while point)
            //noinspection unchecked
            var r = (Map<Integer, Float>) Serializer.deserialize(br, Map.class);
            model.movies.stream()
                    .filter(m -> r.keySet().contains(m.getId()))
                    .forEach(m -> m.setUserRating(r.get(m.getId())));
        } catch (IOException e) {
            throw FileException.errorOnRead(ratingFile);
        } catch (Serializer.SerializationException e) {
            // If the deserializer throws something during normal runtime (as opposed to debugging), the file is bad
            throw FileException.badContent(ratingFile, "Rating file contains invalid contents.");
        }
    }

    /**
     * @return movies which have been rated by the user
     */
    public Set<DbMovie> getRatedMovies() {
        return model.movies.stream()
                .filter(m -> !Float.isNaN(m.getUserRating()))
                .filter(m -> m.getUserRating() >= 0 && m.getUserRating() <= 1)
                .collect(Collectors.toSet());
    }

    private boolean containsLike(Collection<String> ss, String s) {
        var ls = s.toLowerCase(Locale.ROOT);
        return ss.stream()
                .map(_s -> _s.toLowerCase(Locale.ROOT))
                .anyMatch(ls::contains);
    }

    // these find* methods find a given DB entry if it matches any of the given strings

    public Set<DbMovie> findMovies(Collection<String> names) {
        return model.movies.stream()
                .filter(a -> containsLike(names, a.getTitle()))
                .collect(Collectors.toSet());
    }

    public Set<DbActor> findActors(Collection<String> names) {
        return model.actors.stream()
                .filter(a -> containsLike(names, a.getName()))
                .collect(Collectors.toSet());
    }

    public Set<DbDirector> findDirectors(Collection<String> names) {
        return model.directors.stream()
                .filter(d -> containsLike(names, d.getName()))
                .collect(Collectors.toSet());
    }

    public Set<DbGenre> findGenres(Collection<String> names) {
        return model.genres.stream()
                .filter(g -> containsLike(names, g.getName()))
                .collect(Collectors.toSet());
    }

    /**
     * Starts a query
     */
    public Query query() {
        return new Query();
    }

    // return values might not be used, but are there to allow method stacking.
    // as it happens one method will always be called last and thus its return value will be unused.
    @SuppressWarnings("UnusedReturnValue")
    public class Query {
        private final Set<DbActor> limitActors = new HashSet<>();
        private final Set<DbGenre> limitGenres = new HashSet<>();
        private final Set<DbDirector> limitDirectors = new HashSet<>();
        private final Set<DbMovie> ratedLike = new HashSet<>();
        private int maxNum = 200;

        public Query preferActor(Collection<DbActor> a) {
            this.limitActors.addAll(a);
            return this;
        }

        public Query preferGenre(Collection<DbGenre> a) {
            this.limitGenres.addAll(a);
            return this;
        }

        public Query preferDirector(Collection<DbDirector> a) {
            this.limitDirectors.addAll(a);
            return this;
        }

        public Query ratedLike(Collection<DbMovie> m) {
            this.ratedLike.addAll(m);
            return this;
        }

        public Query limit(int l) {
            this.maxNum = l;
            return this;
        }

        /**
         * Filters the list of movies by the previously selected actors, directors & genres
         */
        private Set<DbMovie> getRemainingMovieIdsAfterLimitingFactors() {
            BiFunction<Stream<DbMovie>, Set<? extends IMovieRelated>, Stream<DbMovie>> func = (movies, filter) ->
                    movies.filter(m -> filter.stream().allMatch(r -> r.isRelatedToMovie(m)));

            var m = model.movies.stream();
            m = func.apply(m, limitActors);
            m = func.apply(m, limitGenres);
            m = func.apply(m, limitDirectors);

            return m.collect(Collectors.toSet());
        }

        /**
         * Finds movies which people who rated the reference movies also rated and assigns them a rating value
         */
        private Map<DbMovie, Float> otherPeopleWhoLikedXAlsoLiked() {
            // find people who rated [the selected] movies well
            // here, I want to go from the movies to the users that rated them
            var rat = model.ratings.stream()
                    .filter(r -> ratedLike.contains(r.getMovie()))
                    .collect(Collectors.groupingBy(DbRating::getUsername)).entrySet().stream()
                    .map(kv -> {
                        // I need build the average over all movies that this person rated
                        var avg = kv.getValue().stream()
                                .map(r -> {
                                    var f = r.getMovie().getUserRating();
                                    // when there is a user rating, we value the user by how similar their rating is to ours
                                    if (!Float.isNaN(f) && f >= 0 && f <= 1) return 1 - Math.abs(r.getRating() - f);
                                        // otherwise, we take how much they liked the reference film
                                    else return r.getRating();
                                })
                                .collect(Collectors.averagingDouble(Float::doubleValue))
                                .floatValue();
                        return new Pair<>(kv.getKey(), avg);
                    })
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

            // find other movies that these people rated well
            // here I users and want to get other movies that they rated
            return model.ratings.stream()
                    .filter(r -> rat.keySet().contains(r.getUsername()))
                    // multiply the users rating by that users score that was previously calculated
                    .map(r -> new DbRating(r.getUsername(), r.getRating() * rat.get(r.getUsername()), r.getMovie()))
                    .collect(Collectors.groupingBy(DbRating::getMovie)).entrySet().stream()
                    .map(kv -> {
                        // Here I need to build the average over all users that rated this movie
                        var avg = kv.getValue().stream()
                                .map(DbRating::getRating)
                                .collect(Collectors.averagingDouble(Float::doubleValue)).floatValue() *
                                // reduce the rating if the movie was rated by less than 10 people
                                Math.min(kv.getValue().size() / 10f, 1f);
                        return new Pair<>(kv.getKey(), avg);
                    })
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        }

        /**
         * Assigns a rating value to movies based on their overall score
         */
        private Map<DbMovie, Float> otherPeopleGenerallyLike() {
            return model.movies.stream()
                    .map(m -> {
                        // build the average over all ratings that this movie has received
                        var avg = m.getRatings().stream()
                                .map(DbRating::getRating)
                                .collect(Collectors.averagingDouble(Float::doubleValue)).floatValue() *
                                // if a movie has less than 75 ratings, its max score is reduced
                                Math.min(m.getRatings().size() / 75f, 1f);
                        return new Pair<>(m, avg);
                    })
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        }

        /**
         * Combines all given factors to show movie recommendations
         */
        public LinkedHashMap<DbMovie, Float> showRecommendations() {
            var acceptedMovies = getRemainingMovieIdsAfterLimitingFactors();

            Map<DbMovie, Float> likes;
            if (ratedLike.size() > 0) likes = otherPeopleWhoLikedXAlsoLiked();
            else likes = otherPeopleGenerallyLike();

            return likes.entrySet().stream()
                    .filter(like -> acceptedMovies.contains(like.getKey()))
                    // this could use Comparator.comparing(::getValue).reversed(), but that leads to a type error
                    .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                    .limit(maxNum)
                    .map(e -> new Pair<>(e.getKey(), e.getValue()))
                    // using a linked hash map to preserve order
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue, (a, b) -> {
                        throw new RuntimeException("at this point there shouldn't be duplicate keys");
                    }, LinkedHashMap::new));
        }
    }
}
