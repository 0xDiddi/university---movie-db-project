package diddi.test;

import diddi.db.filter.ShitInShitOutFilter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FilterTest {

    @Test
    @DisplayName("Filter filters out a movie with empty title")
    void testFilter() {

        var filter = new ShitInShitOutFilter();

        var model = TestMode.createTestDb();

        assertAll(
                () -> assertDoesNotThrow(() -> filter.setBlacklistFile("unit_test.blacklist.file")),
                () -> {
                    var m = filter.filter(model);
                    assertAll(
                            // this means it threw out the one without title & plot and the one without relations
                            () -> assertEquals(1, m.movies.size())
                    );
                }
        );

    }

}
