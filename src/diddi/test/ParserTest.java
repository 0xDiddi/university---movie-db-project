package diddi.test;

import diddi.parser.Parser;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This is what a proper unit test would look like; testing all edge cases.
 * Most (all) other unit tests will be much less in depth for reasons of time and effort.
 */
@SuppressWarnings("SpellCheckingInspection")
class ParserTest {

    @Test
    @DisplayName("Parser works with good data.")
    void parse() {
        var input = "New_Entity: \"actor_id\",\"actor_name\"\n" +
                "\"16315\",\" Keanu Reeves\"\n" +
                "New_Entity: \"movie_id\",\"movie_title\",\"movie_plot\",\"genre_name\",\"movie_released\"\n" +
                "\"2081\",\"Matrix, The\",\"A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.\",\"Action\",\"1999-03-31\"\n" +
                "New_Entity: \"director_id\",\"director_name\"\n" +
                "\"30192\",\" Lana Wachowski\"\n" +
                "New_Entity: \"actor_id\",\"movie_id\"\n" +
                "\"16315\",\"2081\"\n" +
                "New_Entity: \"director_id\",\"movie_id\"\n" +
                "\"30192\",\"2081\"\n" +
                "New_Entity: \"user_name\",\"rating\",\"movie_id\"\n" +
                "\"Tracey Irwin\",\"5\",\"2081\"";
        Function<Void, BufferedReader> reader = (wtf) -> new BufferedReader(new StringReader(input));

        var opts = new Parser.ParserOpts();

        assertAll(
                () -> assertDoesNotThrow(() -> Parser.parse(reader.apply(null), opts)),
                () -> assertNotNull(Parser.parse(reader.apply(null), opts)),
                () -> {
                    var out = Parser.parse(reader.apply(null), opts);

                    assertAll(
                            () -> assertSame(1, out.movies.size()),
                            () -> assertEquals("Matrix, The", out.movies.values().iterator().next().title),
                            () -> assertSame(1, out.actors.size()),
                            () -> assertEquals("Keanu Reeves", out.actors.values().iterator().next().name),
                            () -> assertSame(1, out.directors.size()),
                            () -> assertEquals("Lana Wachowski", out.directors.values().iterator().next().name)
                    );
                }
        );

    }

    @Test
    @DisplayName("Parser throws on duplicate id.")
    void parseDuplicate() {
        var input = "New_Entity: \"movie_id\",\"movie_title\",\"movie_plot\",\"genre_name\",\"movie_released\"\n" +
                "\"2081\",\"Matrix, The\",\"A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.\",\"Action\",\"1999-03-31\"\n" +
                "\"2081\",\"Matrix, The\",\"A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.\",\"Thriller\",\"1999-03-31\"";
        Function<Void, BufferedReader> reader = (wtf) -> new BufferedReader(new StringReader(input));

        var opts = new Parser.ParserOpts();

        assertAll(
                () -> assertThrows(Parser.ParserException.class,
                        () -> Parser.parse(reader.apply(null), opts),
                        "With genre merging disabled, parser should throw."),
                () -> {
                    var o1 = new Parser.ParserOpts();
                    o1.mergeGenresOnDuplicateMovieId = true;

                    assertDoesNotThrow(() -> Parser.parse(reader.apply(null), o1),
                            "With genre merging enabled, parser shouldn't throw.");

                    var out = Parser.parse(reader.apply(null), o1);
                    assertSame(2, out.movies.values().iterator().next().genre.size(),
                            "Movie with merged genres should have 2 genres.");

                }
        );
    }

    @Test
    @DisplayName("Parser throws on invalid data.")
    void parseInvalid() {
        var input = "\"2081\",\"Matrix, The\",\"A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.\",\"Action\",\"1999-03-31\"";
        Function<Void, BufferedReader> reader = (wtf) -> new BufferedReader(new StringReader(input));

        var opts = new Parser.ParserOpts();

        assertThrows(Parser.ParserException.class,
                () -> Parser.parse(reader.apply(null), opts),
                "Parser throws when in invalid state.");
    }
}