package diddi.test;

import diddi.db.DB;
import diddi.db.model.*;
import diddi.util.io.FileException;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

// when run via the test mode argument (which requires most things to work already)
// we get >= 90% coverage for everything except static and interactive mode and the int argument

public class TestMode {
    /**
     * Performs a series of tests
     *
     * @param db The database to run sample queries against (doesn't affect unit tests)
     * @throws FileException If there was a problem writing the results of the sample queries to the result file
     */
    public static void start(DB db) throws FileException {
        runUnitTests();
        runSampleQueries(db);
    }

    private static void runSampleQueries(DB db) throws FileException {
        var resultFile = new File("result.txt");
        try (var pw = new PrintWriter(new BufferedWriter(new FileWriter(resultFile)))) {
            var q1 = db.query()
                    .preferGenre(db.findGenres(Set.of("Thriller")))
                    .ratedLike(db.findMovies(Set.of("Matrix Revolutions, The")))
                    .limit(10)
                    .showRecommendations();

            pw.println("Query 1: g:'Thriller', m:'Matrix Revolutions, The', l:10");
            for (var a : q1.entrySet())
                pw.println(String.format("%3.2f%%: %s", a.getValue() * 100f, a.getKey().getTitle()));
            pw.println();
            pw.println("---");

            var q2 = db.query()
                    .preferGenre(db.findGenres(Set.of("Adventure")))
                    .ratedLike(db.findMovies(Set.of("Indiana Jones and the Temple of Doom")))
                    .limit(15)
                    .showRecommendations();

            pw.println("Query 2: g:'Adventure', m:'Indiana Jones and the Temple of Doom', l:15");
            for (var a : q2.entrySet())
                pw.println(String.format("%3.2f%%: %s", a.getValue() * 100f, a.getKey().getTitle()));
            pw.println();
            pw.println("---");

            var q3 = db.query()
                    .preferActor(db.findActors(Set.of("Jason Statham", "Keanu Reeves")))
                    .limit(50)
                    .showRecommendations();

            pw.println("Query 3: a:'Jason Statham', a:'Keanu Reeves', l:50");
            for (var a : q3.entrySet())
                pw.println(String.format("%3.2f%%: %s", a.getValue() * 100f, a.getKey().getTitle()));
            pw.println();
            pw.println("---");

            pw.flush();
        } catch (IOException e) {
            throw FileException.errorOnWrite(resultFile);
        }
    }

    private static void runUnitTests() {
        var req = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectPackage("diddi.test"))
                .filters(includeClassNamePatterns(".*Test"))
                .build();

        var lawnchair = LauncherFactory.create();
        var listener = new SummaryGeneratingListener();
        lawnchair.execute(req, listener);
        var summary = listener.getSummary();

        System.out.println(String.format("%d of %d tests passed.", summary.getTestsSucceededCount(), summary.getTestsStartedCount()));
        if (summary.getTestsFailedCount() > 0) {
            System.out.println("Failed tests:");
            summary.getFailures().forEach(f -> {
                System.out.println(f.getTestIdentifier().getDisplayName());
                System.out.println("\t" + f.getException().getMessage());
            });
        }
    }

    // I learn: movie sets must be mutable prior to sorting (no Set.of()) (unless they're empty)
    // and all movie sets must be non-null (but can be empty)
    static ModelTransform.OoModel createTestDb() {
        // invalid
        var m1 = new DbMovie(123, "", "", "sdfsdf");
        m1.fillRelations(mso(null), mso(null), mso(null), mso(null));

        // no entries
        var m2 = new DbMovie(555, "sdfdsf", "sdfsdf", "2019-01-01");
        m2.setUserRating(0.5f);
        m2.fillRelations(mso(null), mso(null), mso(null), mso(null));

        var m3 = new DbMovie(999, "sdfdsf", "fdgfdg", "");

        var a = mso(new DbActor(1, "aaa bbb", mso(m3)));
        var d = mso(new DbDirector(1, "aaa bbb", mso(m3)));
        var g = mso(new DbGenre("test", mso(m3)));
        var r = mso(new DbRating("testuser", 0.5f, m3));

        m3.fillRelations(a, d, g, r);

        return ModelTransform.createModel(Set.of(m1, m2, m3), a, d, g, r);
    }

    // we need this, as Set.of() returns an immutable set which is bad for our case
    private static <T> HashSet<T> mso(T item) {
        var hs = new HashSet<T>();
        if (item != null)
            hs.add(item);
        return hs;
    }

}
